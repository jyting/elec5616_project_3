// savegame.c
// Compile using `gcc -fno-stack-protector savegame.c`

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void dec2bin(int c)
{
    int i = 0;
    for(i = 31; i >= 0; i--){
        if((c & (1 << i)) != 0){
            printf("1");
        }else{
            printf("0");
        } 
    }
    printf("\n");
}

struct Hero {
    unsigned int hp;
    char name[10];
    unsigned int gold;
};

int main(int argc, const char *argv[])
{

    // unsigned int a = 1;
    // char b[2] = "!!";
    // printf("%lu\n, a");
    printf("%d\n", sizeof('\0'));

    struct Hero h;
    h.hp = 30;
    h.gold = 8;

    scanf("%s", h.name);
    dec2bin(h.gold);

    printf("Name: %s\n", h.name);
    // int i = 1;
    // while (h.name[i] != '\0') {
    //     i++;
    // }
    // printf("%d\n", i);
    printf("HP: %d | Gold: %d\n", h.hp, h.gold);

    return 0;
}
